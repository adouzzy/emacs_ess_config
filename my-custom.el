(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes
   (quote
    ("cea3ec09c821b7eaf235882e6555c3ffa2fd23de92459751e18f26ad035d2142" default)))
 '(package-selected-packages
   (quote
    (dashboard counsel swiper tool-bar+ tabbar base16-theme neotree wgrep flx smex flyspell-lazy ess electric-operator expand-region company-math company-statistics company-shell company-quickhelp company flycheck-pos-tip pos-tip flycheck smartparens transpose-frame rainbow-delimiters which-key anzu git-gutter magit counsel-projectile projectile undo-tree use-package ##)))
 '(show-paren-mode t)
 '(tabbar-separator (quote (1))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-tooltip-common ((t (:inherit company-tooltip :weight bold :underline nil))))
 '(company-tooltip-common-selection ((t (:inherit company-tooltip-selection :weight bold :underline nil))))
 '(rainbow-delimiters-depth-1-face ((t (:foreground "LemonChiffon1"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "pale green"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "DarkOrange3"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "steel blue"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "cyan"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "violet"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "light coral"))))
 '(rainbow-delimiters-depth-9-face ((t (:foreground "azure1"))))
 '(rainbow-delimiters-mismatched-face ((t (:inherit rainbow-delimiters-unmatched-face))))
 '(tabbar-modified ((t (:inherit tabbar-default :foreground "red" :box (:line-width 1 :color "white" :style released-button)))))
 '(tabbar-selected ((t (:inherit tabbar-default :foreground "green" :box (:line-width 1 :color "white" :style pressed-button) :height 1.4))))
 '(tabbar-selected-modified ((t (:inherit tabbar-default :foreground "red" :box (:line-width 1 :color "white" :style released-button) :height 1.4)))))
