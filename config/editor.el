
(use-package neotree :ensure t
  :bind ("<f3>" . neotree-toggle)
  :config
  (setq neo-window-width 35) 
  (global-set-key (kbd "C-x C-j") 'direx:jump-to-directory-other-window)
  )

(use-package ivy :ensure t
  :diminish ivy-mode
  :init
  (progn
    (global-set-key (kbd "C-SPC")  #'ivy-switch-buffer)
    (global-set-key (kbd "C-@")  #'ivy-switch-buffer)
    (global-set-key (kbd "C-c C-r")  #'ivy-resume)
    (global-set-key (kbd "C-S-SPC")  #'ivy-switch-buffer)
    (global-set-key (kbd "C-x C-r")  #'ivy-recentf)
    ;; (global-set-key (kbd "C-c C-o") 'ivy-occur)
    ;; (global-set-key (kbd "C-x C-r") 'ivy-resume)

    )
  :config
  (progn
    (ivy-mode t)
    (setq ivy-wrap t)
    (setq ivy-height 8)
    (setq ivy-display-style 'fancy)
    ;; (setq ivy-display-style nil)
    ;; (setq ivy-minibuffer-faces nil)
    (setq ivy-use-virtual-buffers nil)
    (setq ivy-count-format "(%d/%d) ")
    (setq ivy-format-function 'ivy-format-function-default)
    (setq ivy-re-builders-alist
          '(
            ;; (ivy-switch-buffer . ivy--regex-plus)
            (read-file-name-internal . ivy--regex-fuzzy)
            (t . ivy--regex-fuzzy)))
    (setq ivy-initial-inputs-alist nil)
    (ivy-mode t)
    (setq projectile-completion-system 'ivy)
    (setq magit-completing-read-function 'ivy-completing-read)
    (define-key ivy-minibuffer-map (kbd "C-m") 'ivy-alt-done)
    ))
(use-package counsel :ensure t
  :pin melpa
  ;; :commands( counsel-M-x counsel-find-file )
  :bind (
         ( "C-s" . swiper)
         ( "M-x" . counsel-M-x)
         ( "s-x" . counsel-M-x)
         ( "C-h f" . counsel-describe-function)
         ;; ( "C-h K" . counsel-descbinds)
         ( "C-h v" . counsel-describe-variable)
         ( "C-x C-f" . counsel-find-file)
         )
  :config
  (progn
    (use-package smex :ensure t)
    (use-package flx :ensure t)
    (use-package swiper :ensure t)
    (use-package wgrep :ensure t)
    (setq counsel-ag-base-command "ag --nocolor --nogroup %s")
    (setq counsel-rg-base-command "rg -i --no-heading --line-number %s $(git rev-parse --show-toplevel)")
                                        ; (setq counsel-ag-base-command "ag --nocolor --nogroup %s $(git rev-parse --show-toplevel)")
    ;; (setq counsel-ag-base-command "rg -n --color=always --vimgrep --no-heading %s $(git rev-parse --show-toplevel)")
    ;; (setq counsel-ag-base-command "sift --git --no-color --no-group --binary-skip -i %s $(git rev-parse --show-toplevel)")
    (defun my-find-file ()
      "find file if in project, otherwise recent files."
      (interactive)
      (if(projectile-project-p) (counsel-projectile-find-file)
        (counsel-find-file)
        )
      )
    (global-set-key (kbd "C-x C-r") 'counsel-recentf)

                                        ; Use Enter on a directory to navigate into the directory, not open it with dired.

    (global-set-key (kbd "C-s") 'swiper)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    ;; (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    ;; (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    ;; (global-set-key (kbd "<f1> l") 'counsel-load-library)
    ;; (global-set-key (kbd "<f1> k") 'counsel-descbinds)
    ;; (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    ;; (global-set-key (kbd "<f2> u") 'counsel-unicode-char)

    ;;(global-set-key (kbd "C-c g") 'counsel-git)
    ;;(global-set-key (kbd "C-c j") 'counsel-git-grep)
    ;; (global-set-key (kbd "C-c k") 'counsel-ag)
    ;; (global-set-key (kbd "C-x l") 'counsel-locate)


    )
  ) 
(blink-cursor-mode 0)


;; Recentf 
(require 'recentf)
(recentf-mode 1)
(setq-default recentf-max-menu-items 100)
(setq-default recentf-max-saved-items 200)
(run-at-time nil (* 5 60) 'recentf-save-list)

(defalias 'yes-or-no-p 'y-or-n-p)
(setq-default
 ring-bell-function 'ignore
 ;; redisplay-dont-pause t  ; obsolete in 24.5
 split-width-threshold nil
                                        ;tabs
 indent-tabs-mode nil
 tab-always-indent 'complete
 ;; Misc Intendation
 abbrev-mode                    t
 line-spacing                   0
 ispell-program-name "aspell"
 fill-column 80
 )

(setq-default
 frame-title-format                      "%b@emacs"
 inhibit-startup-message                 t
 column-number-mode                      t
 ;; shell-file-name                         "/usr/bin/bash"
 ;; major-mode                      'text-mode
 auto-save-timeout 60
 backup-by-copying t
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 echo-keystrokes 0.1    ; Show unfinished keystrokes early.
 version-control t
 tags-add-tables nil
 ;;bidi-paragraph-direction 'left-to-right'
 backup-directory-alist `((".*" . "~/.emacs.d/tmp/"))
 auto-save-file-name-transforms `((".*" ,"~/.emacs.d/tmp/" t)))
(global-auto-revert-mode t)
;; Save cursor per file
(save-place-mode t)




(use-package base16-theme
  :ensure t
  :config
  (load-theme 'base16-default-dark t))
(eval-after-load "vc" '(remove-hook 'find-file-hooks 'vc-find-file-hook))

(global-set-key (kbd "C-S-c") 'comment-dwim)

(cua-mode t)
(define-key cua-global-keymap [C-return] nil)
(xterm-mouse-mode t)
(setq shift-select-mode t)
(setq tab-width 2)
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)


(global-set-key (kbd "C-s")  'save-buffer)
(use-package swiper :ensure t)
(global-set-key (kbd "C-f") 'swiper)
(global-set-key (kbd "C-S-f") 'counsel-git-grep)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-S-c") 'comment-dwim)
(global-set-key (kbd "C-s") 'save-buffer)
(use-package transpose-frame :ensure t)
