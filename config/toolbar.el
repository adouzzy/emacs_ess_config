;; (add-hook 'after-init-hook
;;           (lambda ()
;;             (define-key global-map [tool-bar magit-button]
;;               '(menu-item "Magit" magit-status
;;                           :image (image :type png :file "~/.emacs.d/logo/magit.png")
;;                           ))))
;; (tool-bar-add-item "magit" 'magit-status 'magit-status :help "magit-status")
(use-package tabbar :ensure t
  :config
  (setq tabbar-use-images t)
 
  (custom-set-faces
   '(tabbar-modified ((t (:inherit tabbar-default :foreground "yellow" :box (:line-width 1 :color "white" :style released-button)))))
   '(tabbar-selected ((t (:inherit tabbar-default :foreground "green" :box (:line-width 1 :color "white" :style pressed-button) :height 1.4))))
   '(tabbar-selected-modified ((t (:inherit tabbar-default :foreground "green" :box (:line-width 1 :color "white" :style released-button) :height 1.4))
                             )
                            ))
  (tabbar-mode t))
(use-package dashboard :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-items '((recents . 5 )
                          (projects . 5)))
  (setq dashboard-banner-logo-title "F3:sidebar F4:Git F7:git-gutter F9:magit-blame")
  )

(define-key-after global-map [menu-bar lorica] (cons "Lorica" (make-sparse-keymap "loricalorica")) 'file)

(define-key global-map [menu-bar lorica a9] '("Comment" .  comment-dwim ))
(define-key global-map [menu-bar lorica a8] '("Git-Blame" . magit-blame ))
(define-key global-map [menu-bar lorica a7] '("Magit" . magit-status ))
(define-key global-map [menu-bar lorica a02] '(menu-item "--single-line" ))
(define-key global-map [menu-bar lorica a6] '("Start R" . R ))
(define-key global-map [menu-bar lorica a01] '(menu-item "--single-line" ))
(define-key global-map [menu-bar lorica a51] '("Search-Project" . counsel-git-grep ))
(define-key global-map [menu-bar lorica a5] '("Save-Project" . projectile-save-project-buffers ))
(define-key global-map [menu-bar lorica a4] '("FileBar" . neotree-toggle ))
(define-key global-map [menu-bar lorica a00] '(menu-item "--single-line" ))
(define-key global-map [menu-bar lorica a3] '("Transpose-frame" . transpose-frame ))
(define-key global-map [menu-bar lorica a2] '("Del-Window" . delete-window ))
(define-key global-map [menu-bar lorica a1] '("Max-Window" . delete-other-windows ))

