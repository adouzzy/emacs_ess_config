(if (version< emacs-version "24.5.1"); Minimum version is emacs 24.5.1
    (error "Your Emacs is too old -- this config requires v%s or higher"))
(setq gc-cons-threshold 99000000)

;; (add-to-list 'load-path (expand-file-name "~/tmp/gnus/lisp/"))
(set-language-environment "utf-8")
(setq custom-file "~/.emacs.d/my-custom.el")
(load custom-file :noerror :nomessage)
(setq debug-on-error nil)
(require 'package)
(setq package-archives
      '(
        ;; ("elpaclone" . "~/elpaclone")
       ("melpa" . "https://melpa.org/packages/") 
;        ("melpa" . "~/elpa_mirror/")
        ("gnu" . "https://elpa.gnu.org/packages/")
        ;;("org" . "http://orgmode.org/elpa/")
        ))

(setq package-archive-priorities
      '(("gnu" . 14)
       ;; ("elpaclone" . 35)
        ("melpa" . 15)))
(defvar my-initialized t  "Avoid initialize twice.") 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fast initialization
(progn 
(setq package-enable-at-startup nil) 
(package-initialize t) 
(let ((default-directory "~/.emacs.d/elpa")) (normal-top-level-add-subdirs-to-load-path)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slow and safe alternative 
;; (package-initialize )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq use-package-enable-imenu-support t)
(unless (require 'use-package nil 'noerror)
  (package-refresh-contents)
  (package-install 'use-package)
  (require 'use-package)
  )
(setq use-package-verbose nil)
(provide 'prepare)
