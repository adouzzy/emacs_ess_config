(use-package ess-site 
  :ensure ess
  ;; ("\\.Rmd\\'" . poly-markdown+r-mode)
  :mode ("\\.[rR]\\'" . R-mode)
  ;; :mode "\\.[rR]\\'\\|\\.[rR]md\\'"
  ;; :mode "\\.R\\'"
  :init(progn
         )
  :config
  (progn
    ;; (add-hook 'R-mode-hook 'flycheck-mode)
    (require 'ess-site)
    
    (add-hook 'ess-mode-hook 
              (lambda()    (run-hooks 'prog-mode-hook)
                ))
    (ess-toggle-underscore nil)
    ;; (setq-default ess-local-customize-alist ess-r-customize-alist) 
    ;; (setq ess-local-customize-alist ess-r-customize-alist)
    (setq ess-nuke-trailing-whitespace-p t
          ess-tab-always-indent nil
          ess-use-auto-complete nil
          ess-r-package-auto-set-evaluation-env nil
          ess-eval-visibly-p	nil
          ess-style 'RStudio
          ess-indent-with-fanct-comments nil
          )
    )
  )

;; Total number of matches
(use-package anzu :ensure t 
  :pin melpa ; melpa-stable
  :defer 4
  ;; :disabled t
  :diminish anzu-mode 
  :config
  (progn
    (global-anzu-mode t)
    ))

(use-package which-key :ensure t
  :diminish which-key-mode
  :pin melpa
  :defer 6
  :config
  (progn
    (which-key-mode t)
    ))
(use-package rainbow-delimiters
  :ensure t 
  :defer 3
  ;; :disabled t
  :config
  (progn
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'R-mode-hook 'rainbow-delimiters-mode)
    (custom-set-faces  
     '(rainbow-delimiters-depth-1-face ((t (:foreground "LemonChiffon1"))))
     '(rainbow-delimiters-depth-2-face ((t (:foreground "pale green"))))
     '(rainbow-delimiters-depth-3-face ((t (:foreground "DarkOrange3"))))
     '(rainbow-delimiters-depth-4-face ((t (:foreground "steel blue"))))
     '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
     '(rainbow-delimiters-depth-6-face ((t (:foreground "cyan"))))
     '(rainbow-delimiters-depth-7-face ((t (:foreground "violet"))))
     '(rainbow-delimiters-depth-8-face ((t (:foreground "light coral"))))
     '(rainbow-delimiters-depth-9-face ((t (:foreground "azure1"))))
     '(rainbow-delimiters-mismatched-face ((t (:inherit rainbow-delimiters-unmatched-face))))
     ) 
    )
  )

(use-package transpose-frame :ensure t 
  :commands (transpose-frame)
  :pin melpa 
  )


(add-hook 'prog-mode-hook 'electric-pair-mode)
(use-package smartparens :ensure t
  :pin melpa ; melpa-stable
  :defer 4
  :diminish smartparens-mode
  :config
  (progn
    (require 'smartparens-config)
    (setq sp-navigate-reindent-after-up nil
          sp-navigate-close-if-unbalanced t
          sp-autoinsert-pair t
          ;; sp-autoescape-string-quote nil
          
          )
    (show-paren-mode t)
    ;; (defun my-test-if-preceeding-is-word (open-pair in-string)
    ;;   (when (string= open-pair "'")
    ;;     (save-excursion
    ;;       (backward-char 1)
    ;;       (cond
    ;;        ((and (not in-string)
    ;;              (memq (preceding-char) '(?r ?u)))
    ;;         nil)
    ;;        (t (looking-back "\\\\sw\\\\|\\\\s_"))))))
    ;; (add-hook 'sp-autoinsert-inhibit-functions 'my-test-if-preceeding-is-word)
    
                                        ; (sp-local-pair 'LaTex-mode "$" "$")
    ;; (smartparens-global-mode t)
    (smartparens-global-mode -1)

    )
  )

(use-package company :ensure t
  :pin melpa
  :diminish company-mode
  :defer 3
  ;; :commands (company-complete)
  :init
  (progn
    ;; (define-key evil-insert-state-map (kbd "TAB") 'company-complete);; replace dabbrev-expand
    (setq company-idle-delay 0.3
          company-minimum-prefix-length 2
          company-require-match nil
          ;; company-dabbrev-ignore-case nil
          ;; company-dabbrev-downcase nil          
          company-tooltip-flip-when-above t
          company-auto-complete nil
          company-dabbrev-code-everywhere t
          company-minimum-prefix-length 3
          company-dabbrev-downcase nil
          company-dabbrev-other-buffers t
          company-auto-complete nil
          company-dabbrev-code-other-buffers 'all
          company-dabbrev-code-everywhere t
          company-dabbrev-code-ignore-case t
          company-clang-prefix-guesser 'company-mode/more-than-prefix-guesser)
    )
  :config
  (progn
    ;; (define-key company-active-map (kbd "TAB") 'company-complete-selection)
    ;; (define-key company-active-map (kbd "<tab>") 'company-complete-selection)
    ;; ;;disable enter
    ;; (define-key company-active-map [return] nil)
    ;; (define-key company-active-map (kbd "RET") nil)
    ;; (define-key company-active-map (kbd "C-n") 'company-select-next)
    ;; (define-key company-active-map (kbd "C-p") 'company-select-previous)
    ;; (define-key company-active-map (kbd "`") 'company-complete-common-or-cycle)
    ;; (define-key company-active-map (kbd "C-M-/") 'company-filter-candidates)
    ;; (define-key company-active-map (kbd "C-d") 'company-show-doc-buffer)

    (custom-set-faces
     '(company-tooltip-common
       ((t (:inherit company-tooltip :weight bold :underline nil))))
     '(company-tooltip-common-selection
       ((t (:inherit company-tooltip-selection :weight bold :underline nil)))))
    (use-package company-quickhelp :ensure t 
      ;; :disabled t 
      :config
      (progn
        (company-quickhelp-mode nil)
        (setq company-quickhelp-delay 2.0
              company-quickhelp-max-lines 40
              )))
    (use-package company-statistics :ensure t
      :config
      (progn
        (add-hook 'after-init-hook #'company-statistics-mode)
        ;; (company-statistics-mode)
        )
      )

    (use-package company-jedi :ensure t :config
      :pin melpa ; melpa-stable
      :disabled t
      (progn
        (defun my/python-mode-hook ()
          (add-to-list 'company-backends 'company-jedi))
        (add-hook 'python-mode-hook 'my/python-mode-hook)
        ))
    (global-company-mode t)
    ))

(use-package expand-region :ensure t
  :pin melpa ; melpa-stable
  :defer 2
  :commands (er/expand-region)
  )

(use-package electric-operator :ensure t
  :defer 3
  :pin melpa
  :config
  (progn
    (add-hook 'R-mode-hook #'electric-operator-mode)
    (add-hook 'python-mode-hook #'electric-operator-mode)
    (electric-operator-add-rules-for-mode 'ess-mode
                                          (cons "%<-%" " %<-% ") ; future
                                          (cons ":=" " := ") ; data.table
                                          (cons "<<-" " <<- ") ; R9
                                          (cons "%>%" " %>% ") ; pipeline
                                          (cons ">>" " %>% ") ; pipeline
                                          (cons "%<>%" " %<>% ") ; pipeline
                                          (cons "%" nil) 
                                          )

    ))



(use-package undo-tree 
  :ensure t
  :defer 1
  :pin melpa ; melpa-stable
  :diminish undo-tree-mode
  :config 
  (progn
    (global-undo-tree-mode)
    (setq undo-tree-auto-save-history t)
    (setq undo-tree-history-directory-alist (quote (("." . "~/.emacs.d/undotree")) ))
    (add-to-list 'undo-tree-incompatible-major-modes 'pdf-view-mode)
    ))

(use-package projectile 
  :ensure t
  :defer 1
  ;; :commands ( helm-projectile-switch-project helm-projectile-find-file helm-ag helm-do-ag-project-root)
  ;; :commands (my-find-file counsel-ag projectile-project-p )
  :diminish projectile-mode
  :config 
  (progn
    (setq projectile-mode-line "Projectile")
    (setq projectile-indexing-method 'alien)
    ;; (setq projectile-switch-project-action 'projectile-dired)
    (setq projectile-enable-caching t)
    :disabled t
    (setq projectile-file-exists-remote-cache-expire nil)
    (setq projectile-file-exists-local-cache-expire (* 600 60))
    (setq projectile-mode-line
          '(:eval (format " P[%s]" (projectile-project-name)))
          )

    (projectile-mode)
    (use-package counsel-projectile :ensure t
      :config
      (progn (setq projectile-completion-system 'ivy)) 
      )

    ))

(setq mouse-wheel-progressive-speed nil)
(setq mouse-autoselect-window nil)

(setq save-abbrevs 'silently)
(setq-default abbrev-mode t)

(require 'winner)
(winner-mode t)
;; Hippie expand
(global-set-key (kbd "C-/") 'hippie-expand);; replace dabbrev-expand
;;(define-key evil-insert-state-map (kbd "C-/") 'hippie-expand);; replace dabbrev-expand
;; ;; (define-key evil-insert-state-map (kbd "S-SPC") 'hippie-expand);; replace dabbrev-expand
;; (define-key evil-insert-state-map (kbd "TAB") 'hippie-expand);; replace dabbrev-expand
(setq hippie-expand-try-functions-list
      '(
        ;;Try yas-snippet 
        ;; yas-hippie-try-expand
        ;; Try to expand word "dynamically", searching the current buffer.
        try-expand-dabbrev
        ;; Try to expand word "dynamically", searching all other buffers.
        try-expand-dabbrev-all-buffers
        ;; Try to expand word "dynamically", searching the kill ring.
        try-expand-dabbrev-from-kill
        )
      )

;; Version Control
(use-package magit :ensure t
  :pin melpa
  :if (not (eq system-type 'cygwin))
  :bind (("<f4>" . magit-status)
         ("<f9>" . magit-blame))
  ;;:commands (magit-status)
  :mode "COMMIT_EDITMSG"
  ;;:defer 1
  :config
  (progn
    (setq magit-save-repository-buffers 'dontask
          ;;magit-revert-buffers-only-for-tracked-files nil
          magit-process-log-max 16
          ;; magit-git-executable "/usr/local/bin/git"
          ;; magit-diff-refine-hunk
          ) 
    ;; (magit-auto-revert-mode nil)
    ;; (setq magit-refresh-status-buffer nil)
    )
  )
(use-package git-gutter :ensure t
  ;; :disabled t
  :commands (git-gutter-mode )
  :config
  (progn
    ;; (custom-set-variables
    ;;  '(git-gutter:modified-sign "  ") ;; two space
    ;;  '(git-gutter:added-sign "++")    ;; multiple character is OK
    ;;  '(git-gutter:deleted-sign "--"))
    (set-face-background 'git-gutter:modified "purple") ;; background color
    (set-face-foreground 'git-gutter:added "green")
    (set-face-foreground 'git-gutter:deleted "red")
    (custom-set-variables
     '(git-gutter:window-width 2)
     '(git-gutter:modified-sign "☁")
     '(git-gutter:added-sign "☀")
     '(git-gutter:deleted-sign "☂"))
    ))

(use-package flycheck :ensure t
  :disabled t
  :defer 5
  :config (progn
            ;; (require 'flycheck)
            (setq flycheck-check-syntax-automatically '(save idle-change mode-enabled))
            (setq flycheck-r-lintr-executable "R")
            (setq flycheck-flake8-maximum-line-length 120)
            (add-hook 'prog-mode-hook 'flycheck-mode)
            (global-flycheck-mode 1)
            (setq flycheck-idle-change-delay 5
                  flycheck-indication-mode 'right-fringe
                  flycheck-error-list-minimum-level 'warning)
            (use-package pos-tip :ensure t
              :pin melpa ; melpa-stable
              )
            (use-package flycheck-pos-tip :ensure t
              :pin melpa ; melpa-stable

              :config(progn
                       (with-eval-after-load 'flycheck
                         (flycheck-pos-tip-mode))
                       ))

            ))

(use-package ispell
  :disabled t
  :defer 8
  :config
  (progn
    (executable-find "aspell")
    (setq ispell-program-name "aspell")
    ;; (setq ispell-alternate-dictionary "/usr/lib/aspell/en-common.rws")
    ;; (setq ispell-alternate-dictionary "/home/zzhou/.emacs.d/english-words.txt")
    (setq ispell-dictionary "english")
    (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))

    (when (eq system-type'darwin)
      (setq ispell-program-name "aspell")
      (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))
      )
    )
  )
