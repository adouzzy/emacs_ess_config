(package-initialize)
(require 'cl)
;; no need to byte compile
(require 'bytecomp)
(let ((file-name-handler-alist nil) (gc-cons-threshold 99000000))
  (add-to-list 'load-path (expand-file-name "~/.emacs.d/config" user-emacs-directory))

  (defvar my-init-list '(
                         "~/.emacs.d/config/prepare.el"
                         "~/.emacs.d/config/ess.el"
                         "~/.emacs.d/config/editor.el"
                         "~/.emacs.d/config/toolbar.el"
                         )
    "Subconfigs to be loaded."
    )

  ;; (loop for file in my-init-list do (byte-recompile-file file nil 0 t ))
  (loop for file in my-init-list do 
        (progn
          (load-file file)
          )
        )
  )


